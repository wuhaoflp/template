package cn.wuhao.template.utils;

/**
 * 
 * @Description 常量定义
 * @Author wuhao
 * @Date 2014年6月11日
 * 
 */
public class Constant {

	public static final String SEP = System.getProperty("file.separator");
	
	public static final String CONFIG_PATH = "/home/conf/";

	// public static final String CONFIG_PATH = "D://conf/";

}
