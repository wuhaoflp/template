package cn.wuhao.template.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @Description 属性配置读取工具
 * @Author wuhao
 * @Date 2014年6月11日
 * 
 */
public class PropertyUtil {

	private static final Log log = LogFactory.getLog(PropertyUtil.class);
	private static Properties pros = new Properties();

	// 加载属性文件
	static {
		try {
			File configFile = new File(Constant.CONFIG_PATH + "config.properties");
			FileInputStream inputStream = new FileInputStream(configFile);
			pros.load(inputStream);
		} catch (Exception e) {
			log.error("load configuration error", e);
		}
	}

	/**
	 * 读取配置文中的属性值
	 * 
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return pros.getProperty(key);
	}

	public static void main(String[] args) {
		log.info("hello");
		System.out.println(getProperty("zk.zkConnect"));
	}
}
